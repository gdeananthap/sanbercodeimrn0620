// Tugas 6 - Object - Gde Anantha Priharsena - 22 Juni 2020

// Soal 1
var now = new Date();
var thisYear = now.getFullYear();// 2020 (tahun sekarang)

function arrayToObject(arr) {
    // Code di sini
    var n = arr.length;
    if (n>0){
        for (var i = 0; i<n ; i++){
            var data = {};
            data.firstName = arr[i][0];
            data.lastName = arr[i][1];
            data.gender = arr[i][2];
            if (arr[i][3]>thisYear || arr[i][3] == null){
                data.age = "Invalid Birth Year";
            }else{
                data.age = thisYear - arr[i][3];
            }
            var result = `${i+1}. ${data.firstName} ${data.lastName}: `;
            console.log(result,data);
        }
    }else {
        console.log('""');
    }
}

// Driver Code
var people = [ ["Bruce", "Banner", "male", 1975], ["Natasha", "Romanoff", "female"] ];
arrayToObject(people);
/*
    1. Bruce Banner: { 
        firstName: "Bruce",
        lastName: "Banner",
        gender: "male",
        age: 45
    }
    2. Natasha Romanoff: { 
        firstName: "Natasha",
        lastName: "Romanoff",
        gender: "female".
        age: "Invalid Birth Year"
    }
*/

var people2 = [ ["Tony", "Stark", "male", 1980], ["Pepper", "Pots", "female", 2023] ];
arrayToObject(people2);
/*
    1. Tony Stark: { 
        firstName: "Tony",
        lastName: "Stark",
        gender: "male",
        age: 40
    }
    2. Pepper Pots: { 
        firstName: "Pepper",
        lastName: "Pots",
        gender: "female".
        age: "Invalid Birth Year"
    }
*/
 
// Error case 
arrayToObject([]); // ""


// Soal 2
function shoppingTime(memberId, money) {
    // you can only write your code here!
    var barang = ['Sepatu Stacattu', 'Baju Zoro', 'Baju H&N', 'Sweater Uniklooh', 'Casing Handphone'];
    var harga = [1500000, 500000, 250000, 175000, 50000];
    if (memberId == null || memberId == ""){
        return ("Mohon maaf, toko X hanya berlaku untuk member saja");
    }else if(money<50000){
        return ("Mohon maaf, uang tidak cukup");
    }else{
        var data = {};
        data.memberId = memberId;
        data.money = money;
        data.listPurchased = [];
        data.changeMoney = money;
        for(var i = 0; i<5 ; i++){
            if (harga[i] > data.changeMoney){
                continue;
            }else{
                data.changeMoney -= harga[i];
                data.listPurchased.push(barang[i]);
            }
        }
        return (data);
    }
  }
   
// TEST CASES
console.log(shoppingTime('1820RzKrnWn08', 2475000));
//{ memberId: '1820RzKrnWn08',
// money: 2475000,
// listPurchased:
//  [ 'Sepatu Stacattu',
//    'Baju Zoro',
//    'Baju H&N',
//    'Sweater Uniklooh',
//    'Casing Handphone' ],
// changeMoney: 0 }

console.log(shoppingTime('82Ku8Ma742', 170000));
//{ memberId: '82Ku8Ma742',
// money: 170000,
// listPurchased:
//  [ 'Casing Handphone' ],
// changeMoney: 120000 }

console.log(shoppingTime('', 2475000)); //Mohon maaf, toko X hanya berlaku untuk member saja
console.log(shoppingTime('234JdhweRxa53', 15000)); //Mohon maaf, uang tidak cukup
console.log(shoppingTime()); ////Mohon maaf, toko X hanya berlaku untuk member saja

// Soal 3
function naikAngkot(arrPenumpang) {
    var rute = ['A', 'B', 'C', 'D', 'E', 'F'];
    //your code here
    var result = [];
    var n = arrPenumpang.length;
    for(var i = 0; i<n ; i++ ){
        var data = {};
        data.penumpang = arrPenumpang[i][0];
        data.naikDari = arrPenumpang[i][1];
        data.tujuan = arrPenumpang[i][2];
        data.bayar = (rute.indexOf(arrPenumpang[i][2]) - rute.indexOf(arrPenumpang[i][1]))*2000
        result.push(data)
    }
    return result
  }
   
//TEST CASE
console.log(naikAngkot([['Dimitri', 'B', 'F'], ['Icha', 'A', 'B']]));
// [ { penumpang: 'Dimitri', naikDari: 'B', tujuan: 'F', bayar: 8000 },
//   { penumpang: 'Icha', naikDari: 'A', tujuan: 'B', bayar: 2000 } ]
   
console.log(naikAngkot([])); //[]