// Tugas 5 - Array - 19 Juni 2020 - Gde Anantha Priharsena

// Soal 1

// Code di sini
function range(startNum, finishNum){
    var result = [];
    if (startNum<finishNum){
        for(var i= startNum; i <= finishNum; i++) {
            result.push(i);
        }
        return result; 
    }else if(startNum > finishNum){
        for(var i= startNum; i >= finishNum ; i-- ){
            result.push(i);
        }
        return result;
    }else{
        return -1;
    }
} 


console.log(range(1, 10)); //[1, 2, 3, 4, 5, 6, 7, 8, 9, 10]
console.log(range(1)); // -1
console.log(range(11,18)); // [11, 12, 13, 14, 15, 16, 17, 18]
console.log(range(54, 50)); // [54, 53, 52, 51, 50]
console.log(range()); // -1 

console.log();

// Soal 2

// Code di sini
function rangeWithStep(startNum, finishNum, step){
    var result = [];
    if (startNum<finishNum){
        for(var i= startNum; i <= finishNum; i+=step) {
            result.push(i);
        }
        return result; 
    }else if(startNum > finishNum){
        for(var i= startNum; i >= finishNum ; i-=step ){
            result.push(i);
        }
        return result;
    }else{
        return -1;
    }
} 
 
console.log(rangeWithStep(1, 10, 2)); // [1, 3, 5, 7, 9]
console.log(rangeWithStep(11, 23, 3)); // [11, 14, 17, 20, 23]
console.log(rangeWithStep(5, 2, 1)); // [5, 4, 3, 2]
console.log(rangeWithStep(29, 2, 4)); // [29, 25, 21, 17, 13, 9, 5]

console.log();

// Soal 3

// Code di sini
function sum(startNum, finishNum, step){
    if (step == null){
        step =   1 ;
    } // Jika parameter ke-3 tidak diisi maka stepnya adalah 1
    
    var result = 0;
    
    if (startNum < finishNum){
        for(var i= startNum; i <= finishNum; i+=step) {
            result+=i;
        }
        return result; 
    }else if(startNum > finishNum){
        for(var i= startNum; i >= finishNum ; i-=step ){
            result += i;
        }
        return result;
    }else if (startNum != null){
        return startNum;
        // Kasus jika tidak ada parameter ke-2 dan ke-3
    }else{
        return result;
        // Kasus jika tidak ada ketiga parameter
    }
}
console.log(sum(1,10)); // 55
console.log(sum(5, 50, 2)); // 621
console.log(sum(15,10)); // 75
console.log(sum(20, 10, 2)); // 90
console.log(sum(1)); // 1
console.log(sum()); // 0 

console.log();

// Soal 4

var input = [
    ["0001", "Roman Alamsyah", "Bandar Lampung", "21/05/1989", "Membaca"],
    ["0002", "Dika Sembiring", "Medan", "10/10/1992", "Bermain Gitar"],
    ["0003", "Winona", "Ambon", "25/12/1965", "Memasak"],
    ["0004", "Bintang Senjaya", "Martapura", "6/4/1970", "Berkebun"]
] 

function dataHandling(input){
    for (var i = 0 ; i < input.length ; i++){
        console.log(`Nomor ID:  ${input[i][0]}`);
        console.log(`Nama Lengkap:  ${input[i][1]}`);
        console.log(`TTL:  ${input[i][2]} ${input[i][3]}`);
        console.log(`Hobi:  ${input[i][4]}`);
        console.log();
    }
}
dataHandling(input);

// Soal 5

function balikKata(word){
    var result="";
    for (var i = word.length - 1 ; i>= 0; i--){
        result+=(word[i]);
    }
    return result;
}
 
console.log(balikKata("Kasur Rusak")); // kasuR rusaK
console.log(balikKata("SanberCode")); // edoCrebnaS
console.log(balikKata("Haji Ijah")); // hajI ijaH
console.log(balikKata("racecar")); // racecar
console.log(balikKata("I am Sanbers")); // srebnaS ma I 
console.log();

// Soal 6

var input = ["0001", "Roman Alamsyah ", "Bandar Lampung", "21/05/1989", "Membaca"];

function dataHandling2(input){
    input.splice(1, 2, "Roman Alamsyah Elsharawy", "Provinsi Bandar Lampung");
    input.splice(4, 1, "Pria", "SMA Internasional Metro");
    console.log(input);

    var tanggal = input[3].split("/");
    var bulan = parseInt(tanggal[1]);
    switch(bulan) {
        case 1:   { console.log("Januari"); break; }
        case 2:   { console.log("Februari"); break; }
        case 3:   { console.log("Maret"); break; }
        case 4:   { console.log("April"); break; }
        case 5:   { console.log("Mei"); break; }
        case 6:   { console.log("Juni"); break; }
        case 7:   { console.log("Juli"); break; }
        case 8:   { console.log("Agustus"); break; }
        case 9:   { console.log("September"); break; }
        case 10:  { console.log("Oktober"); break; }
        case 11:  { console.log("November"); break; }
        case 12:  { console.log("Desember"); break; }
        default:  { console.log('Bulan tidak valid'); }}
    
    var tanggal2 = tanggal.slice();
    tanggal2.sort(function (value1, value2) { return (parseInt(value2) - parseInt(value1)) } );
    console.log(tanggal2);

    var new_tanggal = tanggal.join("-");
    console.log(new_tanggal);
    
    var nama = input[1].slice(0,15);
    console.log(nama);
}

dataHandling2(input)