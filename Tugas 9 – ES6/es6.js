// Tugas 9 - ES6 - Gde Anantha Priharsena - 25 Juni 2020

// Soal 1
const golden = () => {
    console.log("this is golden!!");
}
golden();

console.log();

// Soal 2

const newFunction = (firstName, lastName) => {
    return {
        firstName,
        lastName,
        fullName : () => {
            console.log(`${firstName} ${lastName}`);
            return;
        }
    }
}
newFunction("William", "Imoh").fullName();

console.log();

// Soal 3
const newObject = {
    firstName: "Harry",
    lastName: "Potter Holt",
    destination: "Hogwarts React Conf",
    occupation: "Deve-wizard Avocado",
    spell: "Vimulus Renderus!!!"
}

const {firstName, lastName, destination, occupation, spell} = newObject;
// Driver code
console.log(firstName, lastName, destination, occupation);
console.log();

// Soal 4
const west = ["Will", "Chris", "Sam", "Holly"];
const east = ["Gill", "Brian", "Noel", "Maggie"];
const combined = [...west, ...east];
//Driver Code
console.log(combined);
console.log()

// Soal 5
const planet = "earth"
const view = "glass"
let before = `Lorem ${view}dolor sit amet, consectetur adipiscing elit,${planet}do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam`
 
// Driver Code
console.log(before)