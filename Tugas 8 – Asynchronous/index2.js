var readBooksPromise = require('./promise.js')
 
var books = [
    {name: 'LOTR', timeSpent: 3000}, 
    {name: 'Fidas', timeSpent: 2000}, 
    {name: 'Kalkulus', timeSpent: 4000}
]
 
// Lanjutkan code untuk menjalankan function readBooksPromise
function readNextBooks(index, times){
    readBooksPromise(times, books[index])
        .then(function(fullfilled){
            readNextBooks(index+1, fullfilled);
        })
        .catch(function(error){
            return error;
        })
}

readNextBooks(0,10000);