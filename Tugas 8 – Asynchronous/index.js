// di index.js
var readBooks = require('./callback.js')
 
var books = [
    {name: 'LOTR', timeSpent: 3000}, 
    {name: 'Fidas', timeSpent: 2000}, 
    {name: 'Kalkulus', timeSpent: 4000}
]
 
// Tulis code untuk memanggil function readBooks di sini
function readNextBooks(index, times){
    if (index>books.length - 1){
        return "";
    } else {
        readBooks(times, books[index], function(times){
            return (readNextBooks(index+1, times));
        })
    }
}

readNextBooks(0, 10000);