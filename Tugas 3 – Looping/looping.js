// Tugas 3 - Looping - 17 Juni 2020 - Gde Anantha Priharsena

// Soal 1

var i = 2 ;
console.log('LOOPING PERTAMA');
while (i<=20){
    console.log( i + " - I love coding");
    i += 2;
}
i -= 2;
console.log('LOOPING KEDUA');
while (i>=2){
    console.log( i + " - I will become a mobile developer");
    i -= 2;
}

console.log();

// Soal 2

for(var i = 1; i < 21; i++) {
    if (i%2 == 1){
        if (i%3 == 0){
            console.log( i + " - I Love Coding");
        }
        else{
            console.log( i + " - Santai");
        }
    }
    else{
        console.log(  i + " - Berkualitas");
    }
  }
  
console.log();

// Soal 3

for (var i = 0; i<4 ; i++){
    console.log("########");
}
console.log();

// Soal 4

for (var i = 1; i<8 ; i++){
    console.log("#".repeat(i));
}
console.log();

// Soal 5

for (var i = 1; i<9 ; i++){
    if (i%2 == 1){
        console.log(" # # # #");
    }
    else{
        console.log("# # # #");
    }
}
console.log();
