// Tugas 14 – Components API & Lifecycle - Gde Anantha Priharsena

import React, { Component } from 'react';
import {StyleSheet, Text, View, Image,FlatList } from 'react-native';
import Icon from 'react-native-vector-icons/MaterialIcons';
import SkillItem from './skillItem'
import data from './skillData.json' 

export default class App extends Component {
    render() {
        return (
            <View style={styles.container}>
               <View style={styles.logo}>
                    <Image source={require('./assets/logo.png')} style={{width:187.5, height:51} } />
                </View>
                <View style={styles.userProfile}>
                    <Icon name='person-pin' size={26} color='#3EC6FF' />
                    <View style={styles.userBar}>
                        <Text style={{fontFamily:'Roboto', fontSize:12, color:'#000000'}}>Hai,</Text>
                        <Text style={{fontFamily:'Roboto', fontSize:16, color:'#003366'}}>Mukhlis Hanafi</Text>
                    </View>                
                </View>
                <View style={styles.skillTitle}>
                    <Text style={{fontFamily:'Roboto', fontSize:36, color:'#003366'}}>SKILL</Text>
                    <Text style={styles.line}>                                                                                                                </Text>
                </View>
                <View style={styles.skillCategory}>
                    <View style={styles.skillCategoryBar}>
                        <Text style={styles.skillCategoryItem}>Library/Framework</Text>
                    </View>
                    <View style={styles.skillCategoryBar}>
                        <Text style={styles.skillCategoryItem}>Bahasa Pemrograman</Text>
                    </View>
                    <View style={styles.skillCategoryBar}>
                        <Text style={styles.skillCategoryItem}>Teknologi</Text>
                    </View>
                </View>
                <View style={styles.skillBody}>
                    <FlatList
                    data={data.items}
                    renderItem={(skill)=><SkillItem skill={skill.item}/>}
                    keyExtractor={(item) => item.id}   
                    />
                </View>

            </View>
        );
    }
}




const styles = StyleSheet.create({
    container: {
      flex: 1,
    },
    logo:{
        alignItems: 'flex-start',
        flexDirection:'row',
        justifyContent:'flex-end',
        paddingBottom:3
    },
    userProfile:{
        alignItems: 'center',
        flexDirection:'row',
        justifyContent:'flex-start',
        paddingLeft:19,
        paddingBottom: 16
    },
    userBar:{
        alignItems: 'flex-start',
        flexDirection:'column',
        justifyContent:'flex-start',
        paddingLeft:11
    },
    skillTitle:{
        alignItems: 'flex-start',
        flexDirection:'column',
        justifyContent:'flex-start',
        paddingLeft:16,
        paddingRight:16
    },
    line : {
        borderTopColor: '#3EC6FF', 
        borderTopWidth: 4
    },
    skillCategory:{
        alignItems: 'flex-start',
        flexDirection:'row',
        justifyContent:'space-between',
        paddingLeft:16,
        paddingRight:16
    },
    skillCategoryItem:{
        fontFamily:'Roboto',
        fontSize:12,
        fontWeight:'bold',
        color:'#003366',
        borderRadius:8,
        borderWidth: 2,
        backgroundColor:'#B4E9FF',
        borderColor: '#B4E9FF',
        padding : 11, 
        textAlign:'center'
    },
    skillBody :{
        flex:1,
        paddingTop: 10
    }
});