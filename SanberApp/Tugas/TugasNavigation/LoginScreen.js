// Tugas 13 - Styling & Flexbox - Gde Anantha Priharsena

import React, { Component } from 'react';
import {StyleSheet, Text, View, Image, TouchableOpacity, TextInput } from 'react-native';

export default class App extends Component {
    render() {
        return (
            <View style={styles.container}>
                <View style={styles.logo}>
                    <Image source={require('./assets/logo.png')} style={{width:375, height:102}} />
                </View>

                <View style={styles.login}>
                    <Text style={styles.loginTitle}>Login</Text>
                </View>

                <View style={styles.inputContainer}>
                    <View>
                        <Text style={{color:'#003366'}}>Username / Email</Text>
                        <TextInput style={styles.inputBox}/>                                     
                    </View>
                    <View style={{paddingTop:10}}>
                        <Text style={{color:'#003366'}}>Password</Text>
                        <TextInput style={styles.inputBox}/>
                    </View> 
                </View>
                    
                <View style={styles.buttonBar}>
                    <TouchableOpacity>
                        <Text style={styles.buttonMasuk}>Masuk</Text>
                    </TouchableOpacity>
                    <View >
                        <Text style={styles.atau}>atau</Text>
                    </View>
                    <TouchableOpacity>
                        <Text style={styles.buttonDaftar}>Daftar ?</Text>
                    </TouchableOpacity>
                </View>
            </View>
        );
    }
}




const styles = StyleSheet.create({
    container: {
      flex: 1
    },
    logo :{
        height: 275,
        backgroundColor: '#FFFFFF',
        justifyContent: 'center',
        flexDirection: 'row',
        alignItems: 'center',
    },
    login: {
        alignItems:'center',
        height: 75
    },
    loginTitle: {
        fontSize: 24,
        fontFamily : 'Roboto',
        color : "#003366"
    },
    inputContainer: {
        justifyContent: 'center',
        flexDirection: 'column',
        alignItems: 'center'
    },
    inputBox: {
        borderWidth: 2,
        borderColor: '#003366',
        backgroundColor : '#FFFFFF',
        height: 48,
        width : 294
    },
    buttonBar: {
        justifyContent: 'center',
        flexDirection: 'column',
        alignItems: 'center',
        height: 225
    },
    buttonMasuk : {
        fontFamily:'Roboto',
        fontSize: 24,
        color:'#FFFFFF',
        backgroundColor : '#3EC6FF',
        width : 140,
        height : 40,
        borderRadius: 16,
        borderColor: '#3EC6FF',
        textAlign:'center',
    },
    atau : {
        fontFamily:'Roboto',
        fontSize: 24,
        color:'#3EC6FF',
        paddingTop: 10,
        paddingBottom:10
    },
    buttonDaftar : {
        fontFamily:'Roboto',
        fontSize: 24,
        color:'#FFFFFF',
        backgroundColor : '#003366',
        width : 140,
        height : 40,
        borderRadius: 16,
        borderColor: '#3EC6FF',
        textAlign:'center'
    }
});