import React from "react";
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import {createBottomTabNavigator} from '@react-navigation/bottom-tabs';
import {createDrawerNavigator} from '@react-navigation/drawer';
import Login from './LoginScreen';
import About from './AboutScreen';
import Skill from './SkillScreen';
import Project from './ProjectScreen';
import Add from './AddScreen';

const LoginStack = createStackNavigator();
const LoginStackScreen = () => (
    <LoginStack.Navigator>
        <LoginStack.Screen name="Login" component={Login}/>
    </LoginStack.Navigator>
)

const Tabs = createBottomTabNavigator();
const TabsScreen = () => (
    <Tabs.Navigator>
        <Tabs.Screen name="Skill" component={Skill}/>
        <Tabs.Screen name="Project" component={Project}/>
        <Tabs.Screen name="Add" component={Add}/>
    </Tabs.Navigator>
)

const Drawer = createDrawerNavigator();

export default () => (
    <NavigationContainer>
        <Drawer.Navigator>
            <Drawer.Screen name="Login" component={Login}/>
            <Drawer.Screen name="Tentang Saya" component={About}/>
            <Drawer.Screen name="Profil Pengguna" component={TabsScreen} />
        </Drawer.Navigator>
    </NavigationContainer>
);