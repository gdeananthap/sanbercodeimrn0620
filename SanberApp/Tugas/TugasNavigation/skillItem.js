// Tugas 14 – Components API & Lifecycle - Gde Anantha Priharsena

import React, { Component } from 'react';
import {Platform, StyleSheet, Text, View, Image, TouchableOpacity } from 'react-native';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import Font from 'react-native-vector-icons/FontAwesome5';

export default class SkillItem extends Component {
    render(){
        let skill = this.props.skill;
        return (
            <View style={styles.container}>
                <Icon name={skill.iconName} size={70} color='#003366' />
                <View style={styles.skillDetail}>
                    <Text style={{fontFamily:'Roboto', fontWeight:'bold', fontSize: 24, color:'#003366'}}>{skill.skillName}</Text>
                    <Text style={{fontFamily:'Roboto', fontWeight:'bold', fontSize: 16, color:'#3EC6FF'}}>{skill.categoryName}</Text>
                    <Text style={{fontFamily:'Roboto', fontWeight:'bold', fontSize: 48, color:'#FFFFFF',alignSelf:'flex-end'}}>{skill.percentageProgress}</Text>
                </View>
                <Font name="chevron-right" size={70} color='#003366' />
            </View>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        height: 129,
        backgroundColor:'#B4E9FF',
        justifyContent: 'space-around',
        flexDirection: 'row',
        alignItems: 'center',
        borderRadius : 8,
        marginLeft:14,
        marginRight:18,
        elevation:10,
        marginBottom:10
    },
    skillDetail:{
        width : 170,
        height: 103,
        flexDirection:'column',
        justifyContent:'center',
        alignItems:'flex-start'
    }
});