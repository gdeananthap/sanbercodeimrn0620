// Tugas 13 - Styling & Flexbox - Gde Anantha Priharsena

import React, { Component } from 'react';
import {StyleSheet, Text, View, Image, TouchableOpacity, TextInput } from 'react-native';
import IonIcon from 'react-native-vector-icons/Ionicons';
import Icon from 'react-native-vector-icons/MaterialIcons';
import CommunityIcon from 'react-native-vector-icons/MaterialCommunityIcons';

export default class App extends Component {
    render() {
        return (
            <View style={styles.container}>
                <View style={styles.titleBar}>
                    <Text style={styles.aboutTitle}>Tentang Saya</Text>
                    <Icon name="person-pin" size={200} color="#EFEFEF"/>
                    <Text style={styles.devName}>Mukhlis Hanafi</Text>
                    <Text style={styles.devStatus}>React Native Developer</Text>
                </View>
                <View style={styles.portofolioBar}>
                    <Text style={styles.portofolioContactTitle}>Portofolio</Text>
                    <Text style={styles.line}>                                                                                                          </Text>
                    <View style={styles.portofolioItemBar}>
                        <View style={styles.portofolioItem}>
                            <CommunityIcon name="gitlab" size={42} color="#3EC6FF"/>
                            <Text style={styles.portofolioItemTitle}>@mukhlish</Text>
                        </View>
                        <View style={styles.portofolioItem}>
                            <IonIcon name="logo-github" size={42} color="#3EC6FF"/>
                            <Text style={styles.portofolioItemTitle}>@mukhlis-h</Text>
                        </View>
                    </View>                                                                                                          
                </View>
                <View style={styles.contactBar}>
                    <Text style={styles.portofolioContactTitle}>Hubungi Saya</Text>
                    <Text style={styles.line}>                                                                                                          </Text>
                    <View style={styles.contactItemBar}>
                        <View style={styles.contactEachItemBar}>
                            <IonIcon name='logo-facebook' size={40} color="#3EC6FF"/>
                            <Text style={styles.contactItemTitle}>mukhlis.hanafi</Text>
                        </View>
                        <View style={styles.contactEachItemBar}>
                            <IonIcon name='logo-instagram' size={40} color="#3EC6FF"/>
                            <Text style={styles.contactItemTitle}>@mukhlis_hanafi</Text>
                        </View>
                        <View style={styles.contactEachItemBar}>
                            <IonIcon name='logo-twitter' size={40} color="#3EC6FF"/>
                            <Text style={styles.contactItemTitle}>@mukhlish</Text>
                        </View>
                    </View>                                                                                                         
                
                </View>
            </View>
        );
    }
}




const styles = StyleSheet.create({
    container: {
      flex: 1,
    },
    titleBar : {
        height: 350,
        backgroundColor: '#FFFFFF',
        justifyContent: 'center',
        flexDirection: 'column',
        alignItems: 'center',
        marginTop:50,
    },
    aboutTitle: {
        fontFamily: 'Roboto',
        fontSize:36,
        fontWeight:'bold',
        color: '#003366',
        marginBottom:12
    },
    devName: {
        fontFamily: 'Roboto',
        fontSize: 24,
        fontWeight:'bold',
        color: '#003366',
        marginTop:24
    },
    devStatus: {
        fontFamily: 'Roboto',
        fontSize: 16,
        fontWeight:'bold',
        color: '#3EC6FF',
        marginTop:8
    },
    portofolioBar: {
        height: 140,
        backgroundColor: '#EFEFEF',
        justifyContent: 'flex-start',
        flexDirection: 'column',
        alignItems: 'flex-start',
        borderRadius : 16,
        marginLeft:8,
        marginRight:8,
        marginTop:16
    },
    portofolioContactTitle: {
        fontFamily: 'Roboto',
        fontSize: 18,
        fontWeight:'normal',
        color: '#003366',
        marginTop : 8,
        marginLeft : 16
    },
    line : {
        borderTopColor: '#003366', 
        borderTopWidth: 1,
        marginTop:8,
        marginLeft:16,
        marginRight:16
    },
    portofolioItemBar: {
        backgroundColor: '#EFEFEF',
        justifyContent: 'space-around',
        flexDirection: 'row',
        alignItems:'center',
        marginLeft:8,
        marginRight:8
    },
    portofolioItem:{
        flexDirection:'column',
        justifyContent:'center',
        alignItems:'center',
        marginLeft:50,
        marginRight:50
    },
    portofolioItemTitle: {
        fontFamily: 'Roboto',
        fontSize: 18,
        fontWeight:'bold',
        color: '#003366',
    },
    contactBar: {
        height: 251,
        backgroundColor: '#EFEFEF',
        justifyContent: 'flex-start',
        flexDirection: 'column',
        alignItems: 'flex-start',
        borderRadius : 16,
        marginLeft:8,
        marginRight:8,
        marginTop:9
    },
    contactItemBar: {
        backgroundColor: '#EFEFEF',
        justifyContent: 'space-around',
        flexDirection: 'column',
        alignItems:'flex-start',
        marginLeft:8,
        marginRight:8,
    },
    contactEachItemBar:{
        backgroundColor: '#EFEFEF',
        justifyContent: 'space-around',
        flexDirection: 'row',
        alignItems:'center',
        marginBottom:20,
        marginLeft:89
    },
    contactItemTitle: {
        fontFamily: 'Roboto',
        fontSize: 18,
        fontWeight:'bold',
        color: '#003366',
        marginLeft:19
    },
});