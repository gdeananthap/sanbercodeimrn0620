import React from 'react';
import { StyleSheet, Text, View } from 'react-native';

import YoutubeUI from './Tugas/Tugas12/App';
import LoginScreen from './Tugas/Tugas13/LoginScreen'
import AboutScreen from './Tugas/Tugas13/AboutScreen'
import ToDoApp from './Tugas/Tugas14/App'
import SkillScreen from './Tugas/Tugas14/SkillScreen'
import Index from './Tugas/Tugas15/index'
import IndexNavigation from './Tugas/TugasNavigation/index'
import IndexQuiz3 from './Quiz3/index'

export default function App() {
  return (
    <IndexQuiz3 />
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
});
  